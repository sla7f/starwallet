//
//  SignUpViewController.swift
//  StarWallet
//
//  Created by Mina on 10/12/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    // MARK:- IBOutlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var termsConditionsImageView: UIImageView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var signupButton: UIButton!
    
    // MARK: - Properties
    
    var signUp : SignUpViewController!
    var imageURL : URL?
    
    // MARK: - UIControlelr life cycel
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        signUp = self
        
        // Set style UI
        styleUI()
        
        // Set delegate 
        nameTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Others
    
    func styleUI() {
        view.layoutIfNeeded()
        // Set profile image view circle
        profileImageView.cornerRaduis = profileImageView.width / 2
        // Shadow sign up button
        signupButton.dropShadow()
    }
    
    // MARK:- Buttons action
    
    @IBAction func toggleAcceptTermsConditions(_ sender: UIButton) {
        if termsConditionsImageView.image == #imageLiteral(resourceName: "check_box_check") {
            termsConditionsImageView.image = #imageLiteral(resourceName: "check_box_empty")
        }else {
            termsConditionsImageView.image = #imageLiteral(resourceName: "check_box_check")
        }
    }
    
    @IBAction func imageProfileButtonClicked(_ sender: UIButton) {
        PickImage.showPickerImageController(target: signUp, closure: { (image, url) in
            self.profileImageView.image = image
            self.imageURL = url
        })
    }
    
    @IBAction func signupButtonClicked(_ sender: UIButton) {
        var validationMessage : String = ""
        
        // Validate profile picture
        if profileImageView.image == #imageLiteral(resourceName: "add_photo") {
            validationMessage = "Please choose your profile picture"
        }
        // Validate E-mail
        else if !BlValidation.validateEmail(userEmail: emailTextField.text!) {
            validationMessage = "Please enter a valid email"
        }
        // Validate name
        else if (nameTextField.text?.characters.count)! < 4 {
            validationMessage = "Please set your name"
        }
        // Validate password
        else if !passwordTextField.hasText{
            validationMessage = "Please set your password please"
        }
        
        else if (passwordTextField.text?.characters.count)! < 5 {
            validationMessage = "Please your password must not less than 5"
        }
            
        // Validate matching
        else if passwordTextField.text != confirmPasswordTextField.text {
            validationMessage = "confirm password is not match password"
        }
            
        // Validate terms and codition
        else if termsConditionsImageView.image == #imageLiteral(resourceName: "check_box_empty") {
            validationMessage = "Please accept terms and condition"
        }
        
        // Check if validation message is not empty thus present alert controller to notify the error
        if validationMessage != "" {
            BannerViewController.sharedInstance.presentAlertBanner(title: "Alert", message: validationMessage, backgroundColor: .yellow)
        }else {
            makeSignUp()
        }
    }
    
    // MARK: - Sign up
    
    func makeSignUp() {
        // User model
        let user = User()
        user.name = nameTextField.text
        user.email = emailTextField.text
        user.password = passwordTextField.text
        user.imageUrl = imageURL
        
        // Loading
        LoaderViewController.sharedInstance.showLoader(text: "Register...")
        
        // Call api
        DalSignup().signUP(userModel: user) { (state, messageArray) in
            
            // Dismiss loader
            LoaderViewController.sharedInstance.dismiss()
            
            // Handle response
            if state {
                BannerViewController.sharedInstance.presentAlertBanner(title: "Done", message: "Registration Successflly", backgroundColor: .green)
                self.navigationController?.popViewController(animated: true)
            }else {
                BannerViewController.sharedInstance.presentAlertBanner(title: "Error", message: messageArray[0], backgroundColor: .red)
            }
            
        }
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension SignUpViewController : UITextFieldDelegate {
    
    // MARK: - Tecxtfield delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let length = (textField.text?.characters.count)! + string.characters.count - range.length
        
        // Validate full name
        if textField == nameTextField {
            return BlValidation.validateFullNameTextfield(lenght: length, textFiledText: textField.text!, currentChracterString: string)
        }
        
        return true
    }
}
