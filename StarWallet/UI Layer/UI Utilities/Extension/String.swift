//
//  String.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    // Get numbers from String
    var numbers: String {
        return String(characters.filter { "0"..."9" ~= $0 })
    }
    
    // Detect current character is empty space
    var containsWhitespace: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x20:
                return true
            default:
                continue
            }
        }
        return false
    }
    
    // Detect backspace has pessed
    var detectBackSpace: Bool {
        let  char = self.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        
        if (isBackSpace == -92) {
            print("Backspace was pressed")
            return true
        }
        
        return false
    }
    
    // Subscript
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return self[Range(start ..< end)]
    }
    
    
}
