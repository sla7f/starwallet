//
//  UITextField.swift
//  StarWallet
//
//  Created by Mina on 10/12/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
    
    override open func awakeFromNib() {
        
        // Font size
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            font = font?.withSize((font?.pointSize)! - 3)
            break
        case .iPhone6:
            font = font?.withSize((font?.pointSize)!)
            break
        case .iPhone6Plus:
            font = font?.withSize((font?.pointSize)! + 2)
            break
        default:
            font = font?.withSize((font?.pointSize)!)
        }
    }
}
