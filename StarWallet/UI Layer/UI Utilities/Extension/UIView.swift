//
//  UIView.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    @IBInspectable var cornerRaduis: Float {
        get {
            return Float(self.layer.cornerRadius)
        }
        set {
            self.layer.cornerRadius = CGFloat(newValue)
            self.layer.masksToBounds = true
        }
    }
    
    var width : Float {
        get {
            return Float(self.frame.size.width)
        }
        set {
            self.frame.size.width = CGFloat(newValue)
        }
    }
    
    /**
     *  Drop shadow
     */
    func dropShadow()
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowRadius = 5
    }
}
