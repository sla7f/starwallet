//
//  UILabel.swift
//  StarWallet
//
//  Created by Mina on 10/11/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    override open func awakeFromNib() {
        switch UIDevice().screenType {
        case .iPhone4, .iPhone5:
            font = font.withSize(font.pointSize - 3)
            break
        case .iPhone6:
            font = font.withSize(font.pointSize)
            break
        case .iPhone6Plus:
            font = font.withSize(font.pointSize + 2)
            break
        default:
            font = font.withSize(font.pointSize)
        }
    }
}
