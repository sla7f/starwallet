//
//  SaveFile.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

class File {
    
    func getLocalURL(image: UIImage) -> URL {
        let data                = UIImageJPEGRepresentation(image, 1.0)
        let documentDirectory   = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let photoURL            = NSURL(fileURLWithPath: documentDirectory)
        let localPath           = photoURL.appendingPathComponent("image.JPG")
        do {
            try data?.write(to: localPath!)
        }catch {
            
        }
        return localPath!
    }
}
