//
//  PickImage.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import UIKit

var actionClosure: ((_ image: UIImage,_ url: URL) -> ())?
let imagePicker = PickImage()

class PickImage: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    // MARK: - Properties
    
    static let picker = UIImagePickerController()
    static var target : UIViewController?
    
    
    // MARK: - Presenter
    
    static func showPickerImageController(target: UIViewController, closure:((_ image: UIImage,_ url: URL) -> ())?) {
        
        actionClosure = closure
        
        picker.delegate = imagePicker
        
        let pickerAlert : UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let camerAction : UIAlertAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.fromCamera(target: target)
        }
        let galaryAction : UIAlertAction = UIAlertAction(title: "Galary", style: .default) { (action) in
            self.fromLibrary(target: target)
        }
        let cancelAction : UIAlertAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        pickerAlert.addAction(camerAction)
        pickerAlert.addAction(galaryAction)
        pickerAlert.addAction(cancelAction)
        target.present(pickerAlert, animated: true, completion: nil)
    }
    
    // MARK: - Helper methos
    
    private static func fromLibrary(target: UIViewController) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        target.present(picker, animated: true, completion: nil)
    }
    
    private static func fromCamera(target: UIViewController) {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.camera
        picker.cameraCaptureMode = .photo
        picker.modalPresentationStyle = .fullScreen
        target.present(picker,animated: true,completion: nil)
    }
    
    //MARK: - Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        // Get image URL
        actionClosure?(chosenImage,File().getLocalURL(image: chosenImage))
        picker.dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
