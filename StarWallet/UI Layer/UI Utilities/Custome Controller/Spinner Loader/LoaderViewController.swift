//
//  LoaderViewController.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import SwiftSpinner

private let shared = LoaderViewController()
class LoaderViewController: UIViewController {
    
    // Signleton instance
    class var sharedInstance: LoaderViewController {
        return shared
    }
    
    func showLoader(text: String) {
        SwiftSpinner.show(text)
    }
    
    func dismiss() {
        SwiftSpinner.hide()
    }
}
