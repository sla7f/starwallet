//
//  BannerViewController.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import UIKit
import BRYXBanner

private let shared = BannerViewController()
class BannerViewController: UIViewController {

    class var sharedInstance : BannerViewController{
        return shared
    }
    
    func presentAlertBanner(title: String, message: String, backgroundColor: UIColor) {
        let banner = Banner(title: title, subtitle: message, image: UIImage(named: "Icon"), backgroundColor: backgroundColor)
        banner.dismissesOnTap = true
        banner.show(duration: 3.0)
    }
    

}
