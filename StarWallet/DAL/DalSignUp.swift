//
//  DalSignUp.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation

class DalSignup {
    
    func signUP(userModel:User, callback: @escaping ((_ state: Bool,_  message: [String]) -> ())) {
        // Set paramter
        var params : [String:Any] = [:]
        params[NetwrokConstants.nameKey] = userModel.name
        params[NetwrokConstants.emailKey] = userModel.email
        params[NetwrokConstants.passwordKey] = userModel.password
        params[NetwrokConstants.confirmPasswordKey] = userModel.password
        
        // Set files aprameters
        var files: [String:URL] = [:]
        files[NetwrokConstants.fileKey] = userModel.imageUrl
        
        // Network requester
        NetworkRequester().uploadMultiPartRequester(methodName: NetwrokConstants.registration, params: params, picURLDic: files, callback: { (state, messages) in
            callback(state, messages)
        })
    }
}
