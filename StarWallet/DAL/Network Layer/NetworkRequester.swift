//
//  NetworkRequester.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation
import Alamofire

class NetworkRequester: NSObject {
    
    /**
     * Network Requester
     */
    func uploadMultiPartRequester(methodName: String , params: [String:Any], picURLDic: [String:URL], callback: @escaping (_ state: Bool, _ messgaes: [String]) -> ()) {
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key,value) in params {
                    multipartFormData.append(String(describing: value).data(using: .utf8)!, withName: key)
                }
                
                for (key, value) in picURLDic {
                    multipartFormData.append(value, withName: key)
                }
                
        },
            to: NetwrokConstants.baseURL+NetwrokConstants.registration,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                        let (state, messages) = self.responseHandler(response: response)
                        callback(state, messages)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    callback(false, ["An error occured try again please"])
                }
        }
        )
    }
    
    /**
     *  Handle response
     */
    func responseHandler(response: DataResponse<Any>) -> (Bool, [String]) {
        guard response.result.value != nil else {
            return (false, ["Error"])
        }
        
        let json = response.result.value as! [String:Any]
        
        if json["code"] as! Int == 100 {
            return (true, ["Register Successfully"])
        }
        else {
            var arrOfStringError : [String] = []
            
            let erorrsDic = json["errors"] as! [String: [String]]
            
            for (_,value) in erorrsDic {
                
                for itemValue: String in value {
                    arrOfStringError.append(itemValue)
                }
            }
            
            if arrOfStringError.count > 1 {
                arrOfStringError.remove(at: 0)
            }
            
            return (false, arrOfStringError)
        }
    }
    
}
