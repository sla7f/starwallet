//
//  NetwrokConstants.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation

struct NetwrokConstants {
    
    // Base URL
    static let baseURL = "http://test.star-wallet.com/api"
    
    // Keys
    static let fileKey = "image"
    static let nameKey = "name"
    static let passwordKey = "password"
    static let confirmPasswordKey = "password_confirmation"
    static let emailKey = "email"
    
    // Method 
    static let registration = "/register"
    
}
