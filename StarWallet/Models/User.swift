//
//  User.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation

class User: NSObject {
    
    var name: String!
    var email: String!
    var password: String!
    var imageUrl: URL!
    
    
}
