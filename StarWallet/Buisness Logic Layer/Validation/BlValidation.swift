//
//  BlValidation.swift
//  StarWallet
//
//  Created by Mina on 10/14/17.
//  Copyright © 2017 Mina. All rights reserved.
//

import Foundation

class BlValidation
{
    // MARK: - Validation email helper
    
    static func validateTextStartByAlphabet(fullName: String, minChractNumber: String, maxCharNumber: String) ->Bool {
        do
        {
            let regex = try NSRegularExpression(pattern: "^[A-Za-zء-ي]{\(minChractNumber),\(maxCharNumber)}$", options: .caseInsensitive)
            if regex.matches(in: fullName, options: [], range: NSMakeRange(0, fullName.characters.count)).count > 0  {
                return true}
        }
        catch { return false }
        return false
    }
    
    static func validateCharacterAlphabetOnly(input: String) ->Bool {
        do
        {
            let regex = try NSRegularExpression(pattern: "[A-Za-zء-ي]", options: .caseInsensitive)
            if regex.matches(in: input, options: [], range: NSMakeRange(0, input.characters.count)).count > 0  {
                return true}
        }
        catch { return false }
        return false
    }
    
    class func validateEmail(userEmail: String) -> Bool {
        let emailRegEx = "^([A-Za-z0-9-_])+(\\.([A-Za-z0-9-])+)*@([A-Za-z0-9-])+(\\.+[A-Za-z]{2,4}){1,3}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        // Get text from start to last character before @
        guard userEmail.characters.index(of: "@") != nil else {
            return false
        }
        let textBeforAT: String = userEmail.substring(to: userEmail.characters.index(of: "@")!)
        print(textBeforAT)
        var nextTest: Bool = false
        do
        {
            let regex = try NSRegularExpression(pattern: "[a-zA-Z]", options: .caseInsensitive)
            if regex.matches(in: textBeforAT, options: [], range: NSMakeRange(0, textBeforAT.characters.count)).count > 0  {
                nextTest = true}
        }
        catch { nextTest = false }
        
        for (index, char) in userEmail.characters.enumerated()  {
            if char == "." && userEmail[index+1] == "."{
                nextTest = false
                break
            }
        }
        return (emailTest.evaluate(with: userEmail) && nextTest) ? true : false
    }
    
    
    // MARK: - Text field
    
    static func validateFullNameTextfield(lenght: Int, textFiledText: String, currentChracterString: String) ->Bool {
        if currentChracterString.detectBackSpace {
            return true
        }
        if lenght == 1 {
            guard validateCharacterAlphabetOnly(input: currentChracterString) == true else { return false }
        }
        // Check length of textField text
        if lenght > 20 {
            return false
        }
        // check every chracter is alphabet or whitSapce or backSpace
        if validateCharacterAlphabetOnly(input: currentChracterString) || currentChracterString.containsWhitespace {
            return true
        }
        
        return false
    }
    
    /**
     *  Get country code from local manager
     */
    static func getCountryCodeFromLocalManager() -> String? {
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
            print(countryCode)
            return countryCode
        }
        return nil
    }
    
    static func validatePasswordTextFiled(lenght: Int, textFiledText: String, currentChracterString: String) -> Bool {
        if currentChracterString.detectBackSpace {
            return true
        }
        if lenght > 12 {
            return false
        }
        return true
    }
    
    static func validationEmailTextField(lenght: Int, textFiledText: String, currentChracterString: String) ->Bool {
        if currentChracterString.detectBackSpace {
            return true
        }
        if currentChracterString.containsWhitespace {
            return false
        }
        if lenght > 30 {
            return false
        }
        return true
    }
    
    
}
